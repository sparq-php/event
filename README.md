# Sparq\Event

[![pipeline status](https://gitlab.com/sparq-php/event/badges/master/pipeline.svg)](https://gitlab.com/sparq-php/event/commits/master)
[![Latest Stable Version](https://poser.pugx.org/sparq-php/event/v/stable)](https://packagist.org/packages/sparq-php/event)
[![coverage report](https://gitlab.com/sparq-php/event/badges/master/coverage.svg)](https://gitlab.com/sparq-php/event/commits/master)
[![Total Downloads](https://poser.pugx.org/sparq-php/event/downloads)](https://packagist.org/packages/sparq-php/event)
[![License](https://poser.pugx.org/sparq-php/event/license)](https://packagist.org/packages/sparq-php/event)

A simple to use event class. Add events to any class with a few lines of codes.

### Installation and Autoloading

The recommended method of installing is via [Composer](https://getcomposer.org/).

Run the following command from your project root:

```bash
$ composer require sparq-php/event
```

### Objectives

* Simple to use events
* Easy integration with other packages/frameworks
* Fast and low footprint

### Usage

```php
require_once __DIR__ . "/vendor/autoload.php";

use Sparq\Event\EventTrait;

class Foo
{
	use EventTrait;

	public function __construct()
	{
		$this->emit('eventName');
	}

	public function __set($key, $value)
	{
		$this->emit('set', [$key, $value]);
	}
}

$Foo = new Foo();
$Foo->on('eventName', function() {
	// do something
});
```