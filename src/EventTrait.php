<?php

namespace Sparq\Event;

/*
 * Event Trait.
 */
trait EventTrait
{
    protected $events = [];

    /**
     * Emit event.
     *
     * @param string $event_name
     * @param array  $arguments
     */
    public function emit($event_name, array $arguments = [])
    {
        if (!isset($this->events[$event_name])) {
            return;
        }

        return call_user_func_array($this->events[$event_name], $arguments);
    }

    /**
     * Has Event.
     *
     * @param string $event_name
     */
    public function hasEvent($event_name)
    {
        return isset($this->events[$event_name]);
    }

    /**
     * Event on.
     *
     * @param string   $event_name
     * @param callable $callBack
     *
     * @return bool
     */
    public function on($event_name, callable $callBack)
    {
        $this->events[$event_name] = $callBack;

        return true;
    }

    /**
     * Event off.
     *
     * @param string $event_name
     *
     * @return bool
     */
    public function off($event_name)
    {
        if (!isset($this->events[$event_name])) {
            return false;
        }

        unset($this->events[$event_name]);

        return true;
    }
}
