#!/bin/bash

# Make this file executable
# chmod +x gitlab_docker_install.sh

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
#apt-get install git -yqq

# Install Xdebug
pecl install xdebug
docker-php-ext-enable xdebug

# Composer
composer self-update
composer install --no-progress --no-interaction