<?php

namespace Sparq\Event\Test;

use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    public function testHasMethod()
    {
        $Foo = new Foo();

        $this->assertEquals($Foo->hasEvent('eventName'), false);
    }

    public function testOnMethod()
    {
        $Foo = new Foo();

        $this->assertEquals($Foo->on('beforeSet', function ($key, $value) {
        }), true);
        $this->assertEquals($Foo->on('afterSet', function ($key, $value) {
        }), true);
        $this->assertEquals($Foo->hasEvent('beforeSet'), true);
        $this->assertEquals($Foo->hasEvent('afterSet'), true);
    }

    public function testOffMethod()
    {
        $Foo = new Foo();
        $Foo->on('beforeSet', function ($key, $value) {
        });

        $this->assertEquals($Foo->hasEvent('beforeSet'), true);
        $this->assertEquals($Foo->off('beforeSet'), true);
        $this->assertEquals($Foo->off('eventName'), false);
        $this->assertEquals($Foo->hasEvent('beforeSet'), false);
    }

    public function testEmitMethod()
    {
        $Foo = new Foo();
        $Foo->on('eventName', function () {
        });

        $this->assertEquals($Foo->hasEvent('eventName'), true);
        $this->assertEquals($Foo->emit('eventName'), null);
    }

    public function testEventValueByRefrence()
    {
        $Foo = new Foo();
        $Foo->on('beforeSet', function ($key, &$value) {
            if ('name' === $key) {
                $value = 'beforeSet: '.$value;
            }
        });
        $Foo->set('name', 'My name');

        $this->assertEquals(
            $Foo->get('name'),
            'beforeSet: My name'
        );
    }
}
