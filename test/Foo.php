<?php

namespace Sparq\Event\Test;

use Sparq\Event\EventTrait;

class Foo
{
    use EventTrait;

    private $collection;

    public function set($key, $value)
    {
        $this->emit('beforeSet', [$key, &$value]);

        $this->collection[$key] = $value;

        $this->emit('afterSet', [$key, &$value]);
    }

    public function get($key)
    {
        return (isset($this->collection[$key])) ? $this->collection[$key] : null;
    }
}
